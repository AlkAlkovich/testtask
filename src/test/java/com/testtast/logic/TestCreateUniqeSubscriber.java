package com.testtast.logic;

import com.testtask.controller.SubscriberController;
import com.testtask.logic.CreateUniqeSubscriber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Alk on 24.06.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/application-context.xml"})
public class TestCreateUniqeSubscriber {

    @Autowired
    SubscriberController subscriberRepository;
    @Test
    public void testCreate() throws Exception {
        CreateUniqeSubscriber.create(3,subscriberRepository);
    }
}
