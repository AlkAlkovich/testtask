package com.testtask.repository;

import com.testtask.model.Subscriber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Alk on 24.06.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/application-context.xml"})
public class TestSubscriberRepository {

    @Autowired
    SubscriberRepository subscriberRepository;
    @Test
    public void testSubscriberSave() throws Exception {
        subscriberRepository.save(new Subscriber("test"));
    }

    @Test
    public void testSubscriberGetAll() throws Exception {
        subscriberRepository.getAll();
    }
}
