package com.testtask.repository;

import com.testtask.model.Subscriber;
import com.testtask.model.Traffic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

/**
 * Created by Alk on 24.06.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/application-context.xml"})
public class TestTrafficRepository {

    @Autowired
    TrafficRepository trafficRepository;
    @Autowired
    SubscriberRepository subscriberRepository;
    @Test
    public void testTrafficSave() throws Exception {
        subscriberRepository.save(new Subscriber("test"));
        for (Subscriber subscriber : subscriberRepository.getAll()) {
            trafficRepository.save(new Traffic(23452,new Timestamp(System.currentTimeMillis()),subscriber));
        }
    }

    @Test
    public void testTrafficGetAll() throws Exception {
        trafficRepository.getAll();
    }
}
