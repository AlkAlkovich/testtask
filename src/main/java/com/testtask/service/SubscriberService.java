package com.testtask.service;

import com.testtask.model.Subscriber;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
public interface SubscriberService {
    public void save(Subscriber subscriber);
    List<Subscriber> getAll();
}
