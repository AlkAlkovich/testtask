package com.testtask.service;

import com.testtask.model.Traffic;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
public interface TrafficService {
    public void save(Traffic traffic);
    public List<Traffic> getAll();
}
