package com.testtask.service;

import com.testtask.model.Traffic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.testtask.repository.TrafficRepository;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
@Service
public class TrafficServiceImpl implements TrafficService {

    @Autowired
    private TrafficRepository trafficRepository;

    @Override
    public void save(Traffic traffic) {
        trafficRepository.save(traffic);
    }

    @Override
    public List<Traffic> getAll() {
        return trafficRepository.getAll();
    }
}
