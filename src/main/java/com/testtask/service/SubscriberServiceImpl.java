package com.testtask.service;

import com.testtask.model.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.testtask.repository.SubscriberRepository;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
@Service
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Override
    public void save(Subscriber subscriber) {

        subscriberRepository.save(subscriber);
    }

    @Override
    public List<Subscriber> getAll() {
        return subscriberRepository.getAll();
    }
}
