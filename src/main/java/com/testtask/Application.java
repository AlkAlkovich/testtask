package com.testtask;

import com.testtask.controller.TrafficController;
import com.testtask.logic.AddData;
import com.testtask.model.Traffic;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Alk on 21.06.2015.
 */
public class Application {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/application-context.xml");
        TrafficController trafficController = (TrafficController) applicationContext.getBean("trafficController");
        AddData addData = (AddData) applicationContext.getBean("addData");
        addData.AddBetweenTimeData(args);

        for (Traffic arg : trafficController.getAll()) {
            System.out.println(arg.toString());
        }
    }
}
