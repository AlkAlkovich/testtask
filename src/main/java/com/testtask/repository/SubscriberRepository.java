package com.testtask.repository;

import com.testtask.model.Subscriber;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
public interface SubscriberRepository {

    public void save(Subscriber subscriber);
    List<Subscriber> getAll();

}
