package com.testtask.repository.datajpa;

import com.testtask.LoggerWrapper;
import com.testtask.model.Traffic;
import com.testtask.repository.TrafficRepository;
import com.testtask.repository.proxy.ProxyTrafficRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
@Repository
public class TrafficRepositoryImpl implements TrafficRepository {

    private static final LoggerWrapper LOG = LoggerWrapper.get(TrafficRepositoryImpl.class);
    @Autowired
    private ProxyTrafficRepository proxyTrafficRepository;
    @Override
    public void save(Traffic traffic) {
        LOG.info("Save traffic"+traffic.getDatetime());
        proxyTrafficRepository.save(traffic);
    }

    @Override
    public List<Traffic> getAll() {
        LOG.info("Get all traffic");
        return proxyTrafficRepository.findAll();
    }
}
