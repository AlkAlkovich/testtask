package com.testtask.repository.datajpa;

import com.testtask.LoggerWrapper;
import com.testtask.model.Subscriber;
import com.testtask.repository.SubscriberRepository;
import com.testtask.repository.proxy.ProxySubscriberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
@Repository
public class SubscriberRepositoryImpl implements SubscriberRepository {

    private static final LoggerWrapper LOG = LoggerWrapper.get(SubscriberRepositoryImpl.class);
    @Autowired
    private ProxySubscriberRepository proxySubscriberRepository;

    @Transactional
    public void save(Subscriber subscriber) {
        LOG.info("Save subscriber"+subscriber.getName());
        proxySubscriberRepository.save(subscriber);
    }

    @Override
    public List<Subscriber> getAll() {
        LOG.info("Get all subbsriber");
        return proxySubscriberRepository.findAll();
    }
}
