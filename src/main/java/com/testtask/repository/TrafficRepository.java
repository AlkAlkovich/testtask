package com.testtask.repository;

import com.testtask.model.Traffic;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
public interface TrafficRepository {

    public void save(Traffic traffic);
    List<Traffic> getAll();
}
