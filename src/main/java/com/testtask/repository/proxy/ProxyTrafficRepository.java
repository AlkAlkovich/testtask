package com.testtask.repository.proxy;

import com.testtask.model.Traffic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
public interface ProxyTrafficRepository extends JpaRepository<Traffic,Integer> {

    @Override
    Traffic save(Traffic traffic);

    @Override
    List<Traffic> findAll();
}
