package com.testtask.repository.proxy;

import com.testtask.model.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
public interface ProxySubscriberRepository extends JpaRepository<Subscriber, Integer> {

    @Override
    Subscriber save(Subscriber sub);

    @Override
    List<Subscriber> findAll();
}
