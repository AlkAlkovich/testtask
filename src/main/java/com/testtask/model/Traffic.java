package com.testtask.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Alk on 21.06.2015.
 */
@Entity
@Table(name = "traffic")
public class Traffic extends BaseEntity {
    @Column
    private int data;

    @Column
    private Timestamp datetime;

    @ManyToOne
    @JoinColumn(name="subscriber_id")
    private Subscriber subscriber_id;

    public Traffic() {
    }

    public Traffic(int data, Timestamp datetime) {
        this.data = data;
        this.datetime = datetime;
    }

    public Traffic(int data, Timestamp datetime, Subscriber subscriber_id) {
        this.data = data;
        this.datetime = datetime;
        this.subscriber_id = subscriber_id;
    }

    public Subscriber getSubscriber_id() {
        return subscriber_id;
    }

    @Override
    public String toString() {
        return "Traffic{" +
                "data=" + data +
                ", datetime=" + datetime +
                ", subscriber_id=" + subscriber_id +
                '}';
    }

    public void setSubscriber_id(Subscriber subscriber_id) {
        this.subscriber_id = subscriber_id;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }
}
