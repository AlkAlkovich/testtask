package com.testtask.controller;

import com.testtask.model.Traffic;
import com.testtask.service.TrafficService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Created by Alk on 22.06.2015.
 */
@Controller
public class TrafficController {

    @Autowired
    private TrafficService service;

    public boolean save(Traffic traffic) {
        service.save(traffic);
        return true;
    }

    public List<Traffic> getAll() {
        return service.getAll();
    }

}
