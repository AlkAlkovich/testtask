package com.testtask.controller;

import com.testtask.model.Subscriber;
import com.testtask.service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Created by Alk on 21.06.2015.
 */
@Controller
public class SubscriberController {

    @Autowired
    SubscriberService subscriberService;

    public boolean save(Subscriber subscriber) {
        subscriberService.save(subscriber);
        return true;
    }

    public List<Subscriber> getAll() {
        return subscriberService.getAll();
    }
}
