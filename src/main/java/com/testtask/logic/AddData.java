package com.testtask.logic;

import com.testtask.LoggerWrapper;
import com.testtask.controller.SubscriberController;
import com.testtask.controller.TrafficController;
import com.testtask.model.Subscriber;
import com.testtask.model.Traffic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Alk on 22.06.2015.
 */
@Component
public class AddData  {
    private static final LoggerWrapper LOG = LoggerWrapper.get(AddData.class);
    @Autowired
    SubscriberController subscriberController;
    @Autowired
    TrafficController trafficController;

    public void AddBetweenTimeData(String[] args) {
        LOG.info("Created "+args[0]+" subscribers ");
        CreateUniqeSubscriber.create(Integer.parseInt(args[0]), subscriberController);
        Calendar start = parseDate(args[1]);
        Calendar end = parseDate(args[2]);

        List<Subscriber>subscribers=subscriberController.getAll();
        int i = 0;
        while (!start.equals(end)) {
            start.set(Calendar.MINUTE, i++);
            for (Subscriber subscriber : subscribers) {
                LOG.info("Add traffic");
                trafficController.save(new Traffic((int) (1 + Math.random() * 1000000), new Timestamp(start.getTimeInMillis()), subscriber));
            }
        }
    }

    private Calendar parseDate(String arg) {
        String[] endParse = arg.split("/");
        return new GregorianCalendar(Integer.parseInt(endParse[0]), Integer.parseInt(endParse[1]), Integer.parseInt(endParse[2]), Integer.parseInt(endParse[3]),Integer.parseInt(endParse[4]),0);
    }



}
