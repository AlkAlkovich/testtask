DROP TABLE IF EXISTS traffic;
DROP TABLE IF EXISTS subscriber;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START 100000;

CREATE TABLE subscriber
(
  id   INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  name VARCHAR

);

CREATE TABLE traffic (
  id            INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  subscriber_id INTEGER NOT NULL,
  datetime      TIMESTAMP,
  data          INT,

  FOREIGN KEY (subscriber_id) REFERENCES subscriber (id)
);
